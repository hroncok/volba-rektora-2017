# encoding=utf-8
from __future__ import unicode_literals, print_function

from .common import *
from .app import *

try:
    from .local_settings import *
except ImportError:
    pass
