# encoding=utf-8
"""
Settings needed to run the app. It can run without `RAVEN_CONFIG` or `ANALYTICS_CODE`
"""

from __future__ import unicode_literals, print_function

RAVEN_CONFIG = {
    'dsn': None,
}

ANALYTICS_CODE = None

SOCIAL_AUTH_FIT_KEY = None
SOCIAL_AUTH_FIT_SECRET = None
ALLOWED_TO_VOTE = [
    "B-00000-SUMA-ZAMESTNANEC",
    "B-00000-SUMA-STUDENT"
]
