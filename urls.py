""" URL Configuration
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth.views import LogoutView

from apps.core.views import IndexView
from apps.vote.views import VoteView

urlpatterns = [
    url('', include('social_django.urls', namespace='social')),
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^vote/$', VoteView.as_view(), name='vote'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
]

if settings.DEBUG:  # static is handled with apache on production
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
