"""
WSGI config for production deploy.
"""

import os
import sys

sys.path.append("/var/www/hosts/volbarektora2017.cz/www/volbarektora/")

from django.core.wsgi import get_wsgi_application
from raven import Client
from raven.middleware import Sentry

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

application = get_wsgi_application()

from django.conf import settings

application = Sentry(
    application,
    Client(settings.RAVEN_CONFIG["dsn"])
)
