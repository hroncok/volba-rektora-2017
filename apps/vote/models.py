# encoding=utf-8
from __future__ import unicode_literals, print_function

import uuid

from django.db import models
from django.utils.translation import ugettext_lazy as _


class HasVoted(models.Model):

    id = models.UUIDField(default=uuid.uuid4, primary_key=True, db_index=True)
    user = models.ForeignKey("auth.User")


class Vote(models.Model):

    CHOICES = [
        ("ak", "prof. Ing. Alena Kohoutková, CSc."),
        ("pk", "prof. Ing. Petr Konvalinka, CSc., FEng."),
        ("mp", "prof. Dr. Michal Pěchouček, MSc."),
        ("vp", "doc. RNDr. Vojtěch Petráček, CSc."),
        ("pt", "prof. Ing. Pavel Tvrdík, CSc."),
    ]

    id = models.UUIDField(default=uuid.uuid4, primary_key=True, db_index=True)

    vote = models.CharField(
        choices=CHOICES, verbose_name=_("Chtěl/a bych, aby mým rektorem/mou rektorkou byl/a:"), max_length=2
    )

    student = models.BooleanField(default=False)
    employee = models.BooleanField(default=False)
