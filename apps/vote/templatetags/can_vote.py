# encoding=utf-8
from __future__ import unicode_literals, print_function

from django import template

from apps.vote.models import HasVoted

register = template.Library()


@register.filter
def can_vote(user):
    return not HasVoted.objects.filter(user=user).exists()
