# encoding=utf-8
from __future__ import unicode_literals, print_function

from django import forms

from apps.vote.models import Vote


class CustomRadioSelect(forms.widgets.RadioSelect):
    def options(self, name, value, attrs=None):
        for group in self.optgroups(name, value, attrs):
            for option in group[1]:
                if option["value"]:
                    yield option


class VoteForm(forms.ModelForm):

    class Meta:
        model = Vote
        fields = ["vote"]
        widgets = {
            "vote": CustomRadioSelect
        }