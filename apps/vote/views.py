# encoding=utf-8
from __future__ import unicode_literals, print_function

from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.views.generic import FormView

from apps.fitauth import get_roles
from apps.vote.forms import VoteForm
from apps.vote.models import HasVoted


class VoteView(FormView):

    form_class = VoteForm
    template_name = "vote.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            messages.error(request, _("Před hlasováním se musíte přihlásit."))
            return redirect(reverse("index"))

        if HasVoted.objects.filter(user=request.user):
            messages.error(request, _("Již už jste jednou volili."))
            return redirect(reverse("index"))

        return super(VoteView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        instance = form.save(commit=False)
        if HasVoted.objects.filter(user=self.request.user).exists():
            messages.error(self.request, _("Již už jste jedou volili."))
            return redirect(reverse("index"))

        roles = get_roles(self.request.user)

        instance.employee = "B-00000-ZAMESTNANEC" in roles
        instance.student = "B-00000-STUDENT" in roles
        instance.save()

        try:
            HasVoted.objects.create(user=self.request.user)
        except:
            instance.delete()
            messages.error(self.request, _("Nastala nějaká chyba, prosíme, hlasujte znova."))
            return super(VoteView, self).form_invalid(form)

        messages.success(self.request, _("Hlas byl uložen."))
        return redirect(reverse("index"))
