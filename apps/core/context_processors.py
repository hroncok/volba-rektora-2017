from django.conf import settings


def analytics_code(request):
    return {
        "analytics_code": settings.ANALYTICS_CODE
    }
