import json
from collections import defaultdict

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import TemplateView
from math import ceil
from social_django.models import UserSocialAuth

from apps.fitauth import get_roles, get_faculties
from apps.vote.models import Vote, HasVoted


class IndexView(TemplateView):
    template_name = "index.html"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            roles = get_roles(request.user)
            badroles = not len((set(roles) & set(settings.ALLOWED_TO_VOTE)))
            if badroles:
                logout(request)
                messages.error(request, "Podle Usermapu nejste student ani zaměstnanec ČVUT. Pokud si myslíte, že je to"
                                        " špatně, kontaktujte nás přes GitLab (odkaz je v patičce).")

                if request.method == "POST":
                    return redirect(reverse("index"))

        return super(IndexView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(IndexView, self).get_context_data(**kwargs)

        votes_count = Vote.objects.count()
        students_count = Vote.objects.filter(student=True).count()
        employees_count = Vote.objects.filter(employee=True).count()

        votes = {}
        votes_students = {}
        votes_employees = {}
        for code in ["ak", "pk", "mp", "vp", "pt"]:
            votes[code] = Vote.objects.filter(vote=code).count()
            votes_students[code] = Vote.objects.filter(student=True, vote=code).count()
            votes_employees[code] = Vote.objects.filter(employee=True, vote=code).count()

        candidates = [
            {
                "code": "ak",
                "name": "<small>prof. Ing.</small> Alena Kohoutková<small>, CSc.</small>",
                "short_name": "prof. Alena Kohoutková",
                "img": "https://i.imgur.com/0Xj77cC.jpg",
                "links": [
                    ("Strukturovaný životopis", "https://www.cvut.cz/sites/default/files/content/e72f2e93-597b-43ce-990d-f1e5bf567d6e/cs/20171009-strukturovany-zivotopis-ak.pdf"),
                    ("Nominace kandidátky", "https://www.cvut.cz/sites/default/files/content/e72f2e93-597b-43ce-990d-f1e5bf567d6e/cs/20171009-nominace-kandidatky-ak.pdf"),
                    ("Stručná vize ČVUT", "https://www.cvut.cz/sites/default/files/content/e72f2e93-597b-43ce-990d-f1e5bf567d6e/cs/20171009-strucna-vize-cvut-ak.pdf"),
                ],
                "votes": votes["ak"],
                "votes_students": votes_students["ak"],
                "votes_employees": votes_employees["ak"],
                "percentage": (votes["ak"] / votes_count * 100) if votes_count else 0,
                "percentage_students": (votes_students["ak"] / students_count * 100) if students_count else 0,
                "percentage_employees": (votes_employees["ak"] / employees_count * 100) if employees_count else 0,
            },
            {
                "code": "pk",
                "name": "<small>prof. Ing. </small>Petr Konvalinka<small>, CSc., FEng.</small>",
                "short_name": "prof. Petr Konvalinka",
                "img": "https://usermap.cvut.cz/profile/photos/76212ad1-4399-4fc5-bd73-9a7b8aa1e3ec?type=jpg",
                "links": [
                    ("Strukturovaný životopis",
                     "https://www.cvut.cz/sites/default/files/content/e72f2e93-597b-43ce-990d-f1e5bf567d6e/cs/20171009-strukturovany-zivotopis-pk.pdf"),
                    ("Nominace kandidáta",
                     "https://www.cvut.cz/sites/default/files/content/e72f2e93-597b-43ce-990d-f1e5bf567d6e/cs/20171009-nominace-kandidata-pk.pdf"),
                    ("Stručná vize ČVUT",
                     "https://www.cvut.cz/sites/default/files/content/e72f2e93-597b-43ce-990d-f1e5bf567d6e/cs/20171009-strucna-vize-cvut-pk.pdf"),
                ],
                "votes": votes["pk"],
                "votes_students": votes_students["pk"],
                "votes_employees": votes_employees["pk"],
                "percentage": (votes["pk"] / votes_count * 100) if votes_count else 0,
                "percentage_students": (votes_students["pk"] / students_count * 100) if students_count else 0,
                "percentage_employees": (votes_employees["pk"] / employees_count * 100) if employees_count else 0,
            },
            {
                "code": "mp",
                "name": "<small>prof. Dr. </small>Michal Pěchouček<small>, MSc.</small>",
                "short_name": "prof. Michal Pěchouček",
                "img": "https://cs.fel.cvut.cz/upload/persons/e13e86ccb6ad2ff775e2df5631e165cb2e1faf59.jpg",
                "links": [
                    ("Strukturovaný životopis",
                     "https://www.cvut.cz/sites/default/files/content/e72f2e93-597b-43ce-990d-f1e5bf567d6e/cs/20171009-strukturovany-zivotopis-mp.pdf"),
                    ("Nominace kandidáta",
                     "https://www.cvut.cz/sites/default/files/content/e72f2e93-597b-43ce-990d-f1e5bf567d6e/cs/20171009-nominace-kandidata-mp.pdf"),
                    ("Stručná vize ČVUT",
                     "https://www.cvut.cz/sites/default/files/content/e72f2e93-597b-43ce-990d-f1e5bf567d6e/cs/20171009-strucna-vize-cvut-mp.pdf"),
                ],
                "votes": votes["mp"],
                "votes_students": votes_students["mp"],
                "votes_employees": votes_employees["mp"],
                "percentage": (votes["mp"] / votes_count * 100) if votes_count else 0,
                "percentage_students": (votes_students["mp"] / students_count * 100) if students_count else 0,
                "percentage_employees": (votes_employees["mp"] / employees_count * 100) if employees_count else 0,
            },
            {
                "code": "vp",
                "name": "<small>doc. RNDr. </small>Vojtěch Petráček<small>, CSc.</small>",
                "short_name": "doc. Vojtěch Petráček",
                "img": "https://www.cvut.cz/sites/default/files/static/as/photos/petravoj.jpg",
                "links": [
                    ("Strukturovaný životopis",
                     "https://www.cvut.cz/sites/default/files/content/e72f2e93-597b-43ce-990d-f1e5bf567d6e/cs/20171009-strukturovany-zivotopis-vp.pdf"),
                    ("Nominace kandidáta",
                     "https://www.cvut.cz/sites/default/files/content/e72f2e93-597b-43ce-990d-f1e5bf567d6e/cs/20171009-nominace-kandidata-vp.pdf"),
                    ("Stručná vize ČVUT",
                     "https://www.cvut.cz/sites/default/files/content/e72f2e93-597b-43ce-990d-f1e5bf567d6e/cs/20171009-strucna-vize-cvut-vp.pdf"),
                ],
                "votes": votes["vp"],
                "votes_students": votes_students["vp"],
                "votes_employees": votes_employees["vp"],
                "percentage": (votes["vp"] / votes_count * 100) if votes_count else 0,
                "percentage_students": (votes_students["vp"] / students_count * 100) if students_count else 0,
                "percentage_employees": (votes_employees["vp"] / employees_count * 100) if employees_count else 0,
            },
            {
                "code": "pt",
                "name": "<small>prof. Ing. </small>Pavel Tvrdík<small>, CSc.</small>",
                "short_name": "prof. Pavel Tvrdík",
                "img": "https://users.fit.cvut.cz/~tvrdik/profile_photo.jpg",
                "links": [
                    ("Strukturovaný životopis",
                     "https://www.cvut.cz/sites/default/files/content/e72f2e93-597b-43ce-990d-f1e5bf567d6e/cs/20171009-strukturovany-zivotopis-pt.pdf"),
                    ("Nominace kandidáta",
                     "https://www.cvut.cz/sites/default/files/content/e72f2e93-597b-43ce-990d-f1e5bf567d6e/cs/20171009-nominace-kandidata-pt.pdf"),
                    ("Stručná vize ČVUT",
                     "https://www.cvut.cz/sites/default/files/content/e72f2e93-597b-43ce-990d-f1e5bf567d6e/cs/20171009-strucna-vize-cvut-pt.pdf"),
                ],
                "votes": votes["pt"],
                "votes_students": votes_students["pt"],
                "votes_employees": votes_employees["pt"],
                "percentage": (votes["pt"] / votes_count * 100) if votes_count else 0,
                "percentage_students": (votes_students["pt"] / students_count * 100) if students_count else 0,
                "percentage_employees": (votes_employees["pt"] / employees_count * 100) if employees_count else 0,
            }
        ]

        data["candidates"] = candidates
        data["total_votes"] = votes_count
        data["students_count"] = students_count
        data["employees_count"] = employees_count

        data["results"] = sorted(candidates, key=lambda x: x["percentage"], reverse=True)
        data["results_students"] = sorted(candidates, key=lambda x: x["percentage_students"], reverse=True)
        data["results_employees"] = sorted(candidates, key=lambda x: x["percentage_employees"], reverse=True)

        max_percentage = max([x[key] for x in candidates for key in {"percentage",
                                                                     "percentage_students",
                                                                     "percentage_employees"}])

        max_percentage = ceil((max_percentage + 5) / 10) * 10

        data["chart"] = json.dumps([
            {
                "label": "Všechny hlasy",
                "data": [[i + 1, x["percentage"]] for i, x in enumerate(candidates)],
                "bars":
                    {
                        "show": True,
                        "barWidth": 0.2,
                        "order": 1,
                        "lineWidth": 2
                    }
            },
            {
                "label": "Hlasy studentů",
                "data": [[i + 1, x["percentage_students"]] for i, x in enumerate(candidates)],
                "bars":
                    {
                        "show": True,
                        "barWidth": 0.2,
                        "order": 1,
                        "lineWidth": 2
                    }
            },
            {
                "label": "Hlasy zaměstnanců",
                "data": [[i + 1, x["percentage_employees"]] for i, x in enumerate(candidates)],
                "bars":
                    {
                        "show": True,
                        "barWidth": 0.2,
                        "order": 1,
                        "lineWidth": 2
                    },
                "yaxis": 2,
            },
        ])

        data["chart_options"] = json.dumps({
            "xaxes": [{
                "tickLength": 0,
                "min": 0,
                "max": 6,
                "ticks": [[i + 1, x["short_name"]] for i, x in enumerate(candidates)]
            }],
            "yaxes": [
                {"position": "left", "min": 0, "max": max_percentage},
                {"position": "right", "min": 0, "max": max_percentage, "tickLength": 0}
            ],
            "grid": {
                "hoverable": True
            },
            "tooltip": {
                "show": True,
            },
            "legend": {
                "position": "nw",
            }
        })

        users_that_voted = HasVoted.objects.values("user")
        social_auths_of_those_that_voted = UserSocialAuth.objects.filter(user__in=users_that_voted)

        faculties_counts = defaultdict(lambda: 0.0)

        for extra_data in social_auths_of_those_that_voted.values("extra_data"):
            facs = get_faculties(extra_data["extra_data"])

            if len(facs) == 1:
                faculties_counts[facs[0]] += 1
            elif len(facs) > 1:
                for x in facs:
                    faculties_counts[x] += (1 / len(facs))

        data["participation_chart"] = json.dumps(
            [
                {"label": key, "data": val}
                for key, val in sorted(faculties_counts.items(), key=lambda x: x[1], reverse=True)
            ]
        )

        data["participation_options"] = json.dumps({
            "series": {
                "pie": {
                    "innerRadius": 0.5,
                    "show": True
                },
            },
            "grid": {
                "hoverable": True
            },
            "tooltip": {
                "show": True
            }
        })

        return data
