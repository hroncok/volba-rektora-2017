from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.deprecation import MiddlewareMixin
from social_core.exceptions import AuthCanceled, AuthStateMissing
from raven.contrib.django.raven_compat.models import client

from apps.fitauth import NotFoundInUsermapAPI


class CatchAuthCanceledMiddleware(MiddlewareMixin):

    def process_exception(self, request, exception):
        if isinstance(exception, AuthCanceled):
            messages.error(request, "Přihlášení bylo zrušeno")
            return redirect(reverse("index"))
        if isinstance(exception, NotFoundInUsermapAPI):
            messages.error(request, "Omlouváme se, ale nepovedlo se nám Vás najít v Usermap API. "
                                    "Prosíme, zkuste se přihlásit znovu později. Pokud problém přetrvá, "
                                    "prosím vytvořte issue na GitLabu uvedeném v patičce s detaily problému.")
            return redirect(reverse("index"))
        if isinstance(exception, AuthStateMissing):
            client.captureException()
            messages.error(request, "Při přihlašování se vyskytla chyba - pravděpodobně způsobená chybějícím cookie. "
                                    "Ujistěte se prosím, že máte povolené cookies, případně je zkuste před dalším "
                                    "přihlášením promazat. Pokud problém přetrvá, prosím vytvořte issue na "
                                    "GitLabu uvedeném v patičce s detaily problému.")
            return redirect(reverse("index"))
